package UD20_ex8.UD20_ex8;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class UD20_ex8Frame extends JFrame {

	private JPanel contentPane;
	private JTextField inputTextField;
	private JTextField resultTextField;

	//Main donde se ejecutará el frame creado posteriormente
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UD20_ex8Frame frame = new UD20_ex8Frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Frame donde lo configuraremos tal como quiere el ejercicio, y posteriormente será ejecutado en el Main
	public UD20_ex8Frame() {
		
		//Aquí le asigno las configuraciones más basicas, tamaño, bordes, relleno
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 180);
		//Aquí creamos el panel que vamos a utilizar en esta aplicación
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//Aquí hago que la ventana coja el valor de el panel
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		//Aquí creo una etiqueta indicando lo que hay que poner
		JLabel mainLabel = new JLabel("Cantidad a convertir");
		sl_contentPane.putConstraint(SpringLayout.NORTH, mainLabel, 13, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, mainLabel, -291, SpringLayout.EAST, contentPane);
		contentPane.add(mainLabel);
		//Aquí creo un JText Field donde el usuario pondrá los datos que quiere convertir
		inputTextField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputTextField, -3, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputTextField, 6, SpringLayout.EAST, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, inputTextField, -194, SpringLayout.EAST, contentPane);
		contentPane.add(inputTextField);
		inputTextField.setColumns(10);
		//Aquí creo una etiqueta que muestra donde se mostrará el resultado
		JLabel resultLabel = new JLabel("Resultado");
		sl_contentPane.putConstraint(SpringLayout.NORTH, resultLabel, 0, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, resultLabel, 6, SpringLayout.EAST, inputTextField);
		sl_contentPane.putConstraint(SpringLayout.EAST, resultLabel, -126, SpringLayout.EAST, contentPane);
		contentPane.add(resultLabel);
		//Aquí creo un JText Field donde se mostrará el resultado como tal, no puede ser editable para el usuario
		resultTextField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, resultTextField, -3, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, resultTextField, 6, SpringLayout.EAST, resultLabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, resultTextField, -5, SpringLayout.EAST, contentPane);
		resultTextField.setEditable(false);
		contentPane.add(resultTextField);
		resultTextField.setColumns(10);
		//Aquí creo un botón donde convierte el valor del JText Input a pesetas 
		final JButton convertButton = new JButton("Euros a ptas");
		contentPane.add(convertButton);
		//Aquí creo un botón donde cambia el botón de (Euros a psts) a (psts a Euros)
		JButton swapButton = new JButton("Cambiar");
		sl_contentPane.putConstraint(SpringLayout.WEST, swapButton, 328, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, swapButton, -10, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.NORTH, convertButton, 0, SpringLayout.NORTH, swapButton);
		sl_contentPane.putConstraint(SpringLayout.EAST, convertButton, -6, SpringLayout.WEST, swapButton);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, swapButton, -10, SpringLayout.SOUTH, contentPane);
		contentPane.add(swapButton);
		//Aquí creo el botón borrar que al darle click, ejecutará una acción que dejará en null los valores de los JTextFields
		JButton ButtonBorrar = new JButton("Borrar");
		ButtonBorrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				inputTextField.setText("");	
				resultTextField.setText("");
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, ButtonBorrar, 0, SpringLayout.NORTH, convertButton);
		sl_contentPane.putConstraint(SpringLayout.EAST, ButtonBorrar, -6, SpringLayout.WEST, convertButton);
		contentPane.add(ButtonBorrar);
		//Aquí creamos una acción en el botón de Convertir Euroa a pesetas donde dependiendo si está el botón
		//en modo (Euro a peseta) o (Peseta a euro), hará una condición o otra
		convertButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				if (convertButton.getText().equalsIgnoreCase("Euros a ptas")) {
					
					try {
						double parsedNumber = Double.parseDouble(inputTextField.getText());
						Double doubleTest = new Double(parsedNumber);		

						double resultParsedNumber = parsedNumber * 166.386;
						resultTextField.setText(String.valueOf(resultParsedNumber));
						
					}catch(NumberFormatException a) {
						System.out.println("No has puesto un número válido");
					}

				} else {
					
					try {
						double parsedNumber = Double.parseDouble(inputTextField.getText());
						Double doubleTest = new Double(parsedNumber);
			

							double resultParsedNumber = parsedNumber / 166.386;
							
							resultTextField.setText(String.valueOf(resultParsedNumber));
					}catch(NumberFormatException a) {
						System.out.println("No has puesto un número válido");
					}

				}
				
			}		
		});
		//Aquí creamos la acción del botón cambiar donde al darle click cambiará el botón de conversión
		swapButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				if (convertButton.getText().equalsIgnoreCase("Euros a ptas")) {
					
					convertButton.setText("Ptas a euros");
					
				} else {
					
					convertButton.setText("Euros a ptas");
					
				}
				
			}
			
		});
		
	}

}
